
## WHAT NOT TO DO ##

1. Never load JS before CSS

   1. CSS goes in the `<head>`
   
   1. JS goes at the end of `<body>` (except if you really need to load JS first)
   
1. Don’t use `@import` [read more]( #markdown-header-dont-use-import-it-kills-performance )

1. **Don’t bind JS and CSS on a same class** [read more](#markdown-header-javascript-hooks)

1. Only use !important proactively, not actively. [read more](#markdown-header-important)

1. Avoid magic numbers **like the plague**. [read more](#markdown-header-magic-numbers)

1. Do not use entity references. [read more](#markdown-header-entity-references)

1. Don’t specify type when including CSS and JavaScript files as text/css and text/javascript and their respective defaults. [read more](#markdown-header-css-and-javascript-includes)

1. Do not use fixed width and height. Use `max-width` and `min-height` instead.

1. If you use column classes (ex. `.col-12, .col-md-6...`) from the bootstrap grid, you **must** have parent `row`

1. Do **NOT** set `width: 100%` to images. Only `max-width: 100%` to prevent stretching them.  

## GENERAL FORMATTING ##

1. four space indents no tabs / use soft-tabs set to four spaces

1. 80 characters wide columns

1. meaningful use of white space [read more](#markdown-header-meaningful-whitespace)

1. Use only lowercase [read more](#markdown-header-capitalization)

1. Write comments wherever possible [read more](#markdown-header-comments)

1. Write table content [read more](#markdown-header-table-of-contents)

1. Append action items after a colon as in **TODO: action item** [read more](#markdown-header-action-items)


## IMAGES ##

1. Images are lazy loaded (a noscript fallback is always provided)  [read more](#markdown-header-images-optimization)

1. They have alternative text that describes the image [read more](#markdown-header-multimedia-fallback)

1. All images should be optimized (if using the starter scss pack, this is done automatically)

1. Specify inline width and height [read more](#markdown-header-specify-inline-width-and-height) 


## HTML FORMATTING ##

1. Have all the necessary elements in the HEAD element [read more](#markdown-header-head-checklist)

1. Use the HTTPS protocol for embedded resources where possible. [read more](#markdown-header-protocol)

1. For thematic breaks in content use one or two empty lines

1. Separate independent but loosely related snippets of markup with a single empty line [read more](#markdown-header-empty-lines)

1. When quoting attributes values, use DOUBLE quotation marks rather than single. [read more](#markdown-header-quotation-marks)

1. Use `target=”_blank”` with `rel=”noopener”` [read more](#markdown-header-target_blank)

1. HTML attribute order [read more](#markdown-header-attribute-order)

1. Check the links, no broken links allowed

1. Check page for possible issues with HTML


## CSS FORMATTING ##

1. HOW TO WRITE CSS [read more](#markdown-header-how-to-write-css)

1. USE Reset CSS

1. CSS attribute order [read more](#markdown-header-declaration-order)

1. USE BEM - naming convention [read more](#markdown-header-bem-like-naming-block-element-modifier)

1. USE Single Responsibility Principle [read more](#markdown-header-the-single-responsibility-principle)

1. General Rules [read more](#markdown-header-general-rules)

1. Multi-line CSS

1. Splitting CSS across multiple files

1. CSS Naming conventions [read more](#markdown-header-naming-conventions)

1. Avoid specificity [read more](#markdown-header-specificity)

1. Writing table of contents at the start of the CSS file [read more](#markdown-header-table-of-contents)

1. Attempt to align common and related identical strings in declaration [read more](#markdown-header-align-properties)
    
1. In instances where a rule set includes only one declaration, remove line breaks for readability and faster editing. [read more](#markdown-header-single-declarations)

1. Test the page on all current desktop browsers and mobile browsers

1. TIPS for Better CSS [read more](#markdown-header-tips-for-better-css)


## FINAL OPTIMIZATIONS ##

**IMPORTANT**

When the project is done, it **MUST** contain the following:

1. Fonts Optimization [read more](#markdown-header-fonts-optimization)

1. Images Optimization [read more](#markdown-header-images-optimization)

1. Check Open Graph tags [read-more](#markdown-header-open-graph-tags)

1. Set async/defer to scripts [read-more](#markdown-header-asyncdefer)

______________________________________________________________________________
## WHAT NOT TO DO - EXPLANATIONS ##
______________________________________________________________________________


### Don’t use @import - it KILLS performance ###

`@import` loads CSS sequentially instead of parallel and increases round trips. It also lengthens critical path and delays first render.

Compared to `<link>`, `@import` is slower, adds extra page requests and can cause other unforeseen problems. Avoid them and instead go for an alternate approach:

* Use multiple `<link>` elements

* Compile your CSS with a preprocessor like Sass or Less into a single file

### JavaScript Hooks ###

It is unwise to bind CSS and JS onto the same class in HTML. This is because by doing so means that you can’t remove one without the other.  These classes are prepended with js-

`<input type="submit" class="btn js-btn" value="Follow" />`


### !important ###

The word `!important` sends shivers down the spines of almost all front-end developers. It is a way to cheat your way out of specificity ways but it comes at a heavy price. It is viewed as a **last resort**. 

It should be used only as a guarantee, **not** as a fix. **Only use `!important` proactively, not actively.**

### Magic Numbers ###

Magic number is a value that is used only because it works. 

````
.site-nav {
	[styles]
}

.site-nav > li:hover .dropdown {
	position: absolute;
	top: 37px;
	left: 0;
}
````

Here, `top: 37px` is a magic number and the only reason it works is because the li’s inside happen to be 37px tall. But what if the browser renders different height? Or what if someone changes the height of the li? Then that number is not valid and the next dev should now to update it. 

There are a few problems with it:

1. The next dev doesn’t know where the magic number came from so they delete it and are back at square one

1. The next dev is a cautious dev who, because he doesn’t know where the number came from, leaves it there and tries to fix the problem without touching it. That becomes hacking on top of a hack.

**AVOID MAGIC NUMBERS LIKE THE PLAGUE!!!**


### Entity References ###

Do not use entity references. There is no need to use entity references like &mdash; , &rdquo etc. assuming that the same encoding (UTF-8) is used for files and editors. Only exceptions to this rule apply to characters with special meaning in HTML (like < and &) as well as invisible characters.


### CSS and JavaScript includes ###

Per HTML5 spec, there is no need to specify type when including CSS and JavaScript files as text/css and text/javascript and their respective defaults.

````
<!-- External css -->
<link rel="stylesheet" href="code-guide.css">

<style> 
	/* .. */
</style>

<!--Javascript -->
<script src="code-guide.js"></script>
````

________________________________________________________________________________
## GENERAL FORMATTING - EXPLANATIONS ##
_______________________________________________________________________________


### Meaningful Whitespace ###

Use one or two empty lines between new sections

##### Trailing Whitespaces #####

Remove trailing white spaces. They are unnecessary and can complicate diffs.

````
<!--Not recommended -->
<p>What?__</p>
<!--Recommended -->
<p>What?</p>
````

### Capitalization ###

Use only lowercase. This applies to HTML element names, attributes, attribute values, CSS selectors and property values.

````
<!--Not recommended -->
<A HREF="/">Home</A>
<!--Recommended -->
<a href="/">Home</a>

<!--Not recommended -->
color: #E5E5E5;
<!--Recommended -->
color: #e5e5e5;
````

### Comments ###

Explain the code as needed, where possible. Explain what it covers, what purpose does it serve.

### Action Items ###

Mark todos and action items with `TODO`. Highlight todos by using the keyword TODO. Append action items after a colon as in `TODO: action item`.

__________________________________________________________________________________
## IMAGES - EXPLANATIONS ##
__________________________________________________________________________________

### Multimedia Fallback ###

Provide alternative contents for multimedia so everyone can understand what the multimedia content is about.

* Not recommended: `<img src="test.png">`

* Recommended: `<img src="test.png" alt="Test explanation.">`

When not to use `alt="Explanation"` and use only `alt=""`

* Only when the picture doesn’t contain any information for the user, but it is purely decorative

**How to use it:**

Ex. If you have a car on the picture, don’t use `alt=”Car”` since that doesn’t give any information to the user, whether it is a word, a car or something else. Better explanation would be `alt=”A photograph of an abandoned car.”`

**Note**: When you write alt text, always add full stop (.) at the end. The full stop is needed so the screen reader can know that it is the end of the sentance.


### Specify inline width and height

Inline width and height need to be specified so the space where the image will be is occupied before it is loaded, thus preventing content jumps.

        <img src="image.png" alt="Test image." width="500" height="300">


__________________________________________________________________________________
## HTML FORMATTING - EXPLANATIONS ##
__________________________________________________________________________________


### Head Checklist ###

1. **Doctype** - The Doctype is HTML5 and is at the top of your HTML pages

1. **Charset** - The charset is UTF-8

1. **X-UA-compatible** - The X-UA-Compatible meta tag is present

1. **Viewport** - The viewport is declared correctly

1. **Title** - A title is used on all pages

1. **Description** - A meta description is provided, it is unique and doesn’t possess more than 150 characters

1. **Favicons** - Each favicon has been created and displayed correctly

1. All CSS files are loaded before and JS files in the HEAD

1. There is preload for the locally added fonts

1. There is **Facebook Open Graph**

1. There is **Twitter Card**

Check the full checklist [here](https://frontendchecklist.io/)


### Protocol ###

Use the HTTPS protocol for embedded resources where possible.

````
<!-- Not recommended: omits the protocol -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Not recommended: uses the HTTP protocol -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Recommended -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
````

````
<!-- Not recommended: omits the protocol -->
@import '//fonts.googleapis.com/css?family=Open+Sans';

<!-- Not recommended: uses the HTTP protocol -->
@import 'http://fonts.googleapis.com/css?family=Open+Sans';

<!-- Recommended -->
@import 'https://fonts.googleapis.com/css?family=Open+Sans';
````

### Empty Lines

If the elements are independent, add empty line. If they are dependent, remove empty line.

        <ul class="primary-nav">
       
            <li class="primary-nav__item">
                <a href="/" class="primary-nav__link">Home</a>
            </li>
       
            <li class="primary-nav__item">
                <a href="/" class="primary-nav__link">About</a>
       
                <ul class="primary-nav__sub-nav">
                    <li><a href="/contact">Contact</a></li>
                    <li><a href="/products">Products</a></li>
                </ul>
       
            </li>
       
            <li class="primary-nav__item">
               <a href="/" class="primary-nav__link">Home</a>
            </li>
             
        </ul>

### Quotation Marks ###

When quoting attributes values, use DOUBLE quotation marks rather than single.

* Not recommended: `<a class='class'>Sign in</a>`

* Recommended: `<a class="class">Sign in</a>`


### target=”_blank” ###

* Bad UX, but also bad for performance

* New tab’s JS runs on old tab’s thread

* Interference between tabs

* Apply rel="noopener" as well

Check the following site to see the difference with and without noopener:
https://jakearchibald.com/2016/performance-benefits-of-rel-noopener/

````
<!-- will use same thread as the tab that spawned it -->
<a href="#" target="_blank"></a>

<!-- will get its own thread -->
<a href="#" target="_blank" rel="noopener"></a>
````


### Attribute order ###

HTML attributes should come in this particular order for easier reading of code:

1. Class

1. Id, name

1. data-*

1. Src, for, type, href, value

1. Title, alt

1. Role, aria-*

_____________________________________________________________________________________
## CSS FORMATTING - EXPLANATIONS ##
_____________________________________________________________________________________

## How to write CSS ##

1. Begin every new major section of a CSS project with a title. [read more](#markdown-header-titling)

1. Keep CSS selectors short [read more](#markdown-header-keep-css-selectors-short-helps-with)

1. When grouping selectors, each selector goes on a new line;
    
        /* Bad CSS */
        .selector, .selector-secondary, .selector[type=text] {
            padding: 15px;
            margin: 0px 0px 15px;
        }
        
        /* Good CSS */
        .selector,
        .selector-secondary, 
        .selector[type=text] {
            padding: 15px;
            margin: 0 0 15px;
        }
    
1. The opening brace `{` on the same line as our last selector.

1. Include one space before the opening brace of declaration blocks for legibility.

1. Properties and values on the same line.

1. Our first declaration on a new line after our opening brace `{`;

1. A space after our property – value delimiting colon `:`

1. Each declaration on its own new line.

1. A trailing semi-colon `;` on our last declaration.

1. Our closing brace `}` on its own new line;

1. Don’t include spaces after commas within `rgb()`, `rgba()`, `hsl()`, `hsla()` or `rect()` values

1. Lowercase all hex values eg. `#fff` instead of `#FFF`. Lowercase letters are easier to discern when scanning the document.

1. Use shorthand hex values where available ex. `#fff` instead of `#ffffff`.

1. Avoid specifying units for zero values ex. `margin: 0;` instead of `margin: 0px`;


### Declaration Order ###

Related property declarations should be grouped together following the order:

1. Positioning

1. Box model

1. Typographic

1. Visual

Positioning comes first because it can remove an element from the normal flow of the documents and override box model related styles. The box model comes next as it dictates a component’s dimensions and placement.

Everything else takes place inside the component or without impacting the previous two sections, and thus they come last.

````
.declaration-order {
    /* positioning */
    position: absolute;
    top:    0;
    right:  0;
    bottom: 0;
    left:   0;
    /* box-model */
    display: block;
    float: right;
    width:  100px;
    height: 100px;
    /* typography */
    font: normal 13px "Arial", sans-serif;
    line-height: 1.5;
    color: #333;
    text-align: center;
    /* visual */
    background-color: #f5f5f5;
    border: 1px solid #e5e5e5;
    border-radius: 3px;
    /* misc */
    opacity: 1;
````

### BEM-like Naming (Block Element Modifier) ###

* **Block:** The sole root of the component

* **Element:** A component part of the Block (with two underscores __)

* **Modifier:** A variant extension of the Block (with two hyphens --)

Check the documentation on http://getbem.com/

````
.person { }
.person__head { }
.person--tall { }
````

### The Single Responsibility Principle ###

The Single Responsibility Principle is a paradigm that states that all pieces of our code should focus on doing **one thing only**. This means that our CSS should be composed of a series of smaller classes that focus on providing very specific and limited functionality. 

````
.error-message {
    display: block;
    padding: 10px;
    color: #fff;
    font-weight: 700;
    background-color: #f00;
    border-top: 1px solid #f00;
    border-bottom: 1px solid #f00;
}

.success-message {
    display: block;
    padding: 10px;
    color: #fff;
    font-weight: 700;
    background-color: #f00;
    border-top: 1px solid #0f0;
    border-bottom: 1px solid #0f0;
}
````

In the example above we can see that these classes are handling everything, layout, structure and cosmetics. And we have a lot of repetition. We can break these classes into smaller classes:

````
.box {
    display: block;
    padding: 10px;
}

.message {
    font-weight: 700;
    color: #fff;
    border-style: solid;
    border-width: 1px 0;
}

.message--error {
    background-color: #f00;
    border-color: #f00;
}

.message--success {
    background-color: #0f0;
    border-color: #0f0;
}
````

### General Rules ###

Briefly summing up the above sections:

* **Select what you want explicitly,** rather than relying on circumstance or coincidence

* **Write selectors for reusability,** so that you can work more efficiently and reduce waste and repetition.

* **Do not nest selectors unnecessarily,** because this will increase specificity and affect where else you can use your styles

* **Do not qualify selectors unnecessarily,** as this will impact the number of different elements you can apply styles to

* **Keep selectors as short as possible,** in order to keep specificity down and performance up

### Naming Conventions ###

Good naming convention tells: 

* What type of things a class does

* Where a class can be used

* What else a class might be related to.

### Hyphen Delimited ###

All strings in classes are divided with a hyphen (-):

````
.page-head { }

.sub-content { }
````

`.pageHead { }` and `.sub__content { }` are incorrect.

### The Key Selector ###

**The key selector** is the rightmost selector since the browsers read right-to-left. The key selector is often critical in defining a selector’s performance. 

For example if we have `#foo * { }` we might think that this is nice because there can only be one `ID` on a page, but the browser will first read every single node in the `DOM` and then find the specified `ID`, which means this type of selector **should be avoided**.

### Opacity ###

If changing opacity on an element, no matter if on hover or animation or any other way, make sure to write `opacity: 1` on the element where the changes will be applied, even though by default opacity is 1. It helps with repainting elements when the browser loads which improves web page performance.

### Specificity ###

Specificity can:

* Limit your ability to extend and manipulate a codebase

* Interrupt and undo CSS cascading, inheriting nature

* Cause avoidable verbosity in your project

* Prevent things from working as expected when moved into different environments

* Lead to serious developers frustration

Improving specificity can be done with the following changes in our CSS code:

* Not using IDs in your CSS

* Not nesting selectors

* Not qualifying classes

* Not chaining selectors

**Specificity can be wrangled and understood, but it is safer just to avoid it entirely.**


### Hacking Specificity ###

Although we should keep the specificity low, sometimes we encounter problems. There will always be times that we need to hack specificity.

To increase specificity of a class, without making it location dependent, we can chain it with itself.

`.site-nav.site-nav { }`

If we need to style an element that has only an ID, and we already said that we should avoid styling an ID directly because of high specificity, we can do it the following way:

````
<div id="third-party-widget">
    ...
</div>

[id="third-party-widget"] { }
````

Here we are selecting based on an attribute rather than an ID, and the attributes have the same specificity as a class. 

You should keep in mind that these **are hacks** and should not be used if there is any alternative.

### Table of Contents ###

Writing table of contents at the start of the CSS file telling the developers what's in a CSS project, what it does and in what order

````
/*
* CONTENTS
*
* SETTINGS
* Global ----------------------------------- Globally-available variables and config
*
* TOOLS
* Mixins ----------------------------------- Useful mixins
*
* GENERIC
* Normalize.css ---------------------------- A level playing field
* Box-sizing ------------------------------- Better default 'box-sizing'
*
* BASE
* Headings --------------------------------- H1-H6 styles
*
* OBJECTS
* Wrappers --------------------------------- Wrapping and constraining elements
*
* COMPONENTS
* Page-head -------------------------------- the main page header
* Page-foot -------------------------------- the main page footer
* Buttons ---------------------------------- button elements
*
* TRUMPS
* Text ------------------------------------- text helpers
*/
````

### Align Properties ###

Align properties where you can. 

Besides visually looking great, it helps with error detection and multi-line writing.

When using vendor prefixed properties, indent each property such that the declaration’s value lines up vertically for easy multi-line editing.

            .foo {
                -webkit-border-radius: 3px;
                   -moz-border-radius: 3px;
                        border-radius: 3px;
            }
            
            .bar {
                position: absolute;
                top:    0;
                right:  0;
                bottom: 0;
                left:   0;
                margin-right: -10px;
                margin-left:  -10px;
                padding-right: 10px;
                padding-left:  10px;
            }


### Single Declarations ###

In instances where a rule set includes only one declaration, consider removing line breaks for readability and faster editing. Any set with multiple declarations should be split to separate lines. 

The key factor here is error detection. If a CSS validator states you have a syntax error on line 190, and you have a single declaration, you can’t miss it. 

````
/* Single declarations on one line */
.width--sm { width: 60px; }
.width--md { width: 140px; }
.width--lg { width: 220px; }

/* Multiple declarations, one per line */
.sprite {
    display: inline-block;
    width:  16px;
    height: 16px;
    background-image: url('image.png');
}

.icon         { background-position: 0 0; }
.icon-home    { background-position: 0 -20px; }
.icon-account { background-position: 0 -40px; }
````

### Keeping CSS selectors short helps ###

* Increases selector efficiency

* Reduces location dependency

* Increases portability

* Reduces chances of selector breakage

* Decreases specificity

* Can make code more forgiving

### Titling ###

Begin every new major section of a CSS project with a title:

````
/*--------------------------------------
                #SECTION-TITLE
---------------------------------------#

.selector { }
````

The title is prefixed with # to allow us to perform more targeted searches

### Avoid using fixed width and height ###

* Use grid.css whenever possible

* If you need to make custom grid, use max-width

* If you need height of an element, use min-height

* Use fixed width and height only if you truly need it


### TIPS FOR BETTER CSS ###
___________________________________________________________________________

#### Selector Intent ####

It is important when writing CSS that we scope our selectors correctly and selecting the right things for the right reasons. **Selector intent** is the process of deciding what you want to style and how you will go about selecting it. 

**Unwise selector** is `header ul { }`  if we want to style only the site’s main navigation. The example is a poor selector intent because we can have many header elements on our site and if we have more lists in them, all of them will have the same style as the main navigation even if we don’t want that. **This will result in more CSS to undo the greedy nature of a selector like this.** **Better selector** is `.site-nav { }`.


#### Reusability ####

We want the option to be able to move, recycle, duplicate and syndicate components across our projects.


#### Location independence ####

Given the ever-changing nature of most projects, it is better to style things based on **WHAT** they are, not on **WHERE** they are.

* **Bad example:** `.promo a { }`
 
   * We can’t use this selector anywhere else except in a `.promo` container and moreover it will style all links inside it with the same style which will result in more CSS to undo that style
   
* **Better example:** `.btn { }` 

   * It can be reused anywhere outside .promo and will always carry the correct styling
   
#### Portability ####

* **Bad Example:** `input.btn { }`

   * This is a qualified selector. This selector will only work on input elements, and can’t be reused. Better to use more classes which can be reusable.


#### Quasi-Qualified Selectors ####

The qualified selectors are good for signalling where a class might be expected or intended to be used.

* Ex: `ul.nav { }`
   
   * Here we can see that the `.nav class` is intended to be used on an `ul` element. But, by using **quasi-qualified selectors** we can still say the same thing without qualifying the selector: `/*ul*/.nav { }`


#### Naming ####

* We should use sensible names. `.border` or `.red` are never advisable. We should avoid using classes which describe the exact nature of the content and/or its use cases. **Using a class name to describe content is redundant because content describes itself.**

* It is **important** to strike balance between names that do not literally describe the style that a class brings, but also ones that do not explicitly describe specific use cases. 

* Instead of `.home-page-panel` use `.hero`

* Instead of `.site-nav` use `.primary-nav`

* Instead of `.btn-login` use `.btn-primary`

````
/* Runs the risk of becoming out of date; not very maintainable */
.blue { color: blue; }

/* Depends on location in order to be rendered properly */
.header span { color: blue; }

/* Too specific; Limits our ability to reuse */
.header-color { color: blue; }

/* Nicely abstracted, very portable, doesn't risk becoming out of date  */
.highlight-color { color: blue; }
```` 

#### Shorthand notation ####

Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values. Common overused shorthand properties include:

* Padding

* Margin

* Font

* Background

* Border

* Border-radius

Often we don’t need to set all the values a shorthand property represents. For example, HTML headings only set top and bottom margin so when necessary, override only those two values. Excessive use of shorthand properties often leads to sloppier code with unnecessary overrides and unintended side effects.

````
/* Bad example */
.element {
    margin: 0 0 10px;
    background: red;
    background: url('image.png');
    border-radius: 3px 3px 0 0;
}

/* Good example */
.element {
    margin-bottom: 10px;
    background-color: red;
    background-image: url('image.png');
    border-top-left-radius:  3px;
    border-top-right-radius: 3px;
}
````

#### Selector Performance ####

A topic which is - with the quality of today’s browsers - more interesting than important.

Selector performance means how quickly a browser can match the selectors we write in CSS with the nodes it finds in the DOM.

**The longer a selector is, the slower it is.**

`body.home div.header ul { }`

The one above is far less efficient selector than:

`.primary-nav { }`

Why? Because the browsers read CSS selectors **right-to-left.** 

For example, the first example will be read as:

* Find all `ul` elements in the DOM

* Check if they live anywhere inside an element with a class `.header`

* Now check if the `.header` element exists on a `div` element

* Next check if they all live in any element with a class `.home`

* Finally check that `.home` exists on a `body` element

In contrast, the second example will be read as:

* Find all elements with a class `.primary-nav`

_________________________________________________________________________________
## FINAL OPTIMIZATIONS ##
_________________________________________________________________________________

### Fonts Optimization ###

1. Find the google font here https://google-webfonts-helper.herokuapp.com/fonts

1. Select the font weights that are used in the project

1. Copy the generated css and paste it in:

    1. SCSS - `resources/scss/1-tools/_fonts.scss`

    1. CSS - `assets/css/main.css on the top of the file`

1. Download the font files and add them in `assets/fonts/`

1. Add `font-display: swap;` at the end of each `@font-face` to ensure text remains visible during webfont load

1. Run `grunt build`, then `grunt` *for SCSS only

1. If there is a difference between the local fonts and the google fonts

    1. Check if the route in `_fonts.scss` or `main.css` is the correct one

    1. Change css version

1. Remove google fonts from head along with the preconnect for them

1. Check which fonts and font weights are used above the fold

1. Preload the (8) fonts in the head, using the following format (check the path)

`<link rel="preload" href="/assets/fonts/fontname/fontname.woff2" as="font" crossorigin>`

### Images Optimization ###

1. Download the script https://github.com/verlok/vanilla-lazyload

1. Add the `lazyload.min.js` 

    1. SCSS: in `resources/js/`

    1. CSS: in `assets/js/`
    
1. Run `grunt build` then `grunt` *for SCSS only

1. Call the script above the `main.js` file in `includes/scripts.php`

1. Find every image, except the ones above the fold and a section below it, and

1. Add class lazy

1. Replace the attribute src with `data-src`

1. Add the following in

    1. SCSS: `resources/js/main.js`
    
    1. CSS: `assets/js/main.js`

`var lazyLoadInstance = new LazyLoad({ threshold: 800 });`

### Open Graph Tags ###

In the starter packs, there are empty OG tags added in the head. Fill them according to the project needs.

When you finish setting up the content of the OG tags, check the validity on the sites below:

* Facebook Sharing Debugger - https://developers.facebook.com/tools/debug/

* Twitter Card Validator - https://cards-dev.twitter.com/validator

* LinkedIn Post Inspector - https://www.linkedin.com/post-inspector/inspect/

If you want to read more about this, you can check [here](https://ahrefs.com/blog/open-graph-meta-tags/)

### Async/Defer

**async** means that the script is loaded parallel with the other resources, meaning it no longer blocks the DOM. It executes as soon as it is loaded.

**defer** means that the scripts is also loaded parallel with the other resources, meaning it no longer blocks the DOM. The difference with **async** is that it executes only when the DOM is ready. 

When the project is finished, check the scripts and their order, put async/defer where needed without compromising the functionality.

````
<script src="main.js" async></script>

<script src="main.js" defer></scripts>
````

You can read more about this [here](https://medium.com/javascript-in-plain-english/async-and-defer-the-complete-guide-to-loading-javascript-properly-ce6edce1e6b5)
